import 'package:json_annotation/json_annotation.dart';

part 'graphql_models.g.dart';

@JsonSerializable()
class Customer {
  final String id;
  final int balance;
  final String name;
  final List<Offer> offers;

  Customer(
      {required this.id,
      required this.balance,
      required this.name,
      required this.offers});

  factory Customer.fromJson(Map<String, dynamic> json) =>
      _$CustomerFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerToJson(this);
}

@JsonSerializable()
class Offer {
  final String id;
  final int price;
  final Product product;

  Offer({required this.id, required this.price, required this.product});

  factory Offer.fromJson(Map<String, dynamic> json) => _$OfferFromJson(json);

  Map<String, dynamic> toJson() => _$OfferToJson(this);
}

@JsonSerializable()
class Product {
  final String id;
  final String name;
  final String description;
  final String image;

  Product(
      {required this.id,
      required this.name,
      required this.description,
      required this.image});

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
