import 'package:graphql_flutter/graphql_flutter.dart';

const String _getViewerQueryString = """
  query {
    viewer{
    id
    balance
    name
    offers{
      id
      price
      product{
        id
        name
        description
        image
      }
    }
  }
}
""";

Query getViewerQueryWidget(QueryBuilder builder) {
  return Query(
      options: QueryOptions(document: gql(_getViewerQueryString)),
      builder: builder);
}

const String getPurchaseMutationString = """
mutation purchase(\$offerId: ID!){
    purchase(offerId:\$offerId) { 
      errorMessage
      success
  }
  }
""";
