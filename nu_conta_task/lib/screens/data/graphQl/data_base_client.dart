
import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

ValueNotifier<GraphQLClient> getDBClient(){

  final HttpLink dataBaseLink =
  HttpLink("https://staging-nu-needful-things.nubank.com.br/query");

  final AuthLink authLink = AuthLink(
   getToken: () => "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhd2Vzb21lY3VzdG9tZXJAZ21haWwuY29tIn0.cGT2KqtmT8KNIJhyww3T8fAzUsCD5_vxuHl5WbXtp8c");

  final Link link = authLink.concat(dataBaseLink);

  return ValueNotifier(
    GraphQLClient(
      link: link,
      cache: GraphQLCache(
        store: InMemoryStore(),
      ),
    ),
  );
}

