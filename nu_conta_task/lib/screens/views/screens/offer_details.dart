import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nu_conta_task/screens/data/graphQl/queries.dart';
import 'package:nu_conta_task/screens/data/models/graphql_models.dart';
import 'package:nu_conta_task/screens/views/customViews/toast.dart';

class OfferDetailsPage extends StatelessWidget {
  const OfferDetailsPage({Key? key, required this.offer}) : super(key: key);

  final Offer offer;

  Future<void> _onPurchase(BuildContext context, Offer offer) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Mutation(
          options: MutationOptions(
              document: gql(getPurchaseMutationString),
              update: (cache, result) => cache,
              onError: (err) {
                print(err?.graphqlErrors.toString());
              },
              onCompleted: (dynamic resultData) {
                final data = resultData["purchase"];
                  if (data?["success"] == true) {
                    showToast("Item Purchased");
                    Navigator.of(context).pop(context);
                  } else {
                    final String errorMsg =
                        data?["errorMessage"] ?? "An error occurred";
                    showToast(errorMsg);
                  }
                Navigator.of(context).pop(context);
              }),
          builder: ((RunMutation runMutation, QueryResult? result) {
            return AlertDialog(
              title: const Text('Purchase Product'),
              content: Text('Purchase ${offer.product.name}?'),
              actions: <Widget>[
                TextButton(
                  child: const Text('Ok'),
                  onPressed: () {
                    runMutation({'offerId': offer.id});
                  },
                ),
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(offer.product.name),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Column(children: [
            FractionallySizedBox(
                widthFactor: 0.50, child: Image.network(offer.product.image)),
            Container(
              padding: const EdgeInsets.only(top: 15),
              child: Row(children: [
                const Text("Product:",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Container(
                  padding: const EdgeInsets.only(left: 10),
                  child: Expanded(
                    child: Text(offer.product.name,
                        style: const TextStyle(
                            fontSize: 18, overflow: TextOverflow.visible)),
                  ),
                ),
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(top: 15),
              child: Row(children: [
                const Text("Description:",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(offer.product.description,
                        style: const TextStyle(
                            fontSize: 18, overflow: TextOverflow.visible)),
                  ),
                ),
              ]),
            ),
            Container(
              padding: const EdgeInsets.only(top: 15),
              child: Row(children: [
                const Text("Price:",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Container(
                  padding: const EdgeInsets.only(left: 10),
                  child: Expanded(
                    child: Text("${offer.price}\$",
                        style: const TextStyle(
                            fontSize: 18,
                            color: Colors.deepPurple,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ]),
            ),
          Container(
              padding: const EdgeInsets.only(top: 15),
              child: OutlinedButton(
              onPressed: () {
                _onPurchase(context, offer);
              },
              child: const Text('Purchase',
              style: TextStyle(fontSize: 20),),
            )
          )
          ]),
        ));
  }
}
