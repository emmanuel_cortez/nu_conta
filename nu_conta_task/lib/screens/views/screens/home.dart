import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nu_conta_task/screens/data/graphQl/queries.dart';
import 'package:nu_conta_task/screens/data/models/graphql_models.dart';
import 'package:nu_conta_task/screens/views/customViews/offers_list_item.dart';
import 'package:nu_conta_task/screens/views/screens/offer_details.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return getViewerQueryWidget((
      QueryResult result, {
      Refetch? refetch,
      FetchMore? fetchMore,
    }) {
      if (result.hasException) {
        return Center(child: Text(result.exception.toString()));
      }

      if (result.isLoading) {
        return const Center(child: Text('Loading'));
      }

      Customer viewer = Customer.fromJson(result.data!["viewer"]);

      return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Container(
            padding: const EdgeInsets.all(16),
            child: Column(children: [
              Row(children: [
                const Text("Customer:",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Expanded(
                  child: Text(viewer.name,
                      style: const TextStyle(fontSize: 18),
                      textAlign: TextAlign.end),
                ),
              ]),
              Container(
                padding: const EdgeInsets.only(top: 15),
                child: Row(children: [
                  const Text("Balance:",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  Expanded(
                    child: Text("${viewer.balance}\$",
                        style: const TextStyle(fontSize: 18),
                        textAlign: TextAlign.end),
                  ),
                ]),
              ),
              Container(
                padding: const EdgeInsets.only(top: 35),
                alignment: Alignment.centerLeft,
                child: const Text("Offers:",
                    textAlign: TextAlign.start,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ),
              Expanded(
                child: Container(
                    padding: const EdgeInsets.only(top: 15),
                  child: ListView.builder(
                      itemCount: viewer.offers.length,
                      itemBuilder: (BuildContext context, int index) {
                        return
                          GestureDetector(
                            onTap: () { Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => OfferDetailsPage(offer:viewer.offers[index]),
                            )).then((value) => setState(() {}));
                        },
                        child:
                          OffersListItem(viewer.offers[index], index));
                      }),
                ),
              )
            ]),
          ));
    });
  }
}
