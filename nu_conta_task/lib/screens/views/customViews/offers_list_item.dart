import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nu_conta_task/screens/data/models/graphql_models.dart';
import 'package:nu_conta_task/screens/views/screens/offer_details.dart';

class OffersListItem extends StatelessWidget {
  final Offer offer;
  final int index;

  const OffersListItem(this.offer, this.index);

  @override
  Widget build(BuildContext context) {
    return Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                color: Color(0xFFE1D7FD)
              ),
                padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                child: Row(
                    children: [
                  Text("${offer.product.name}:",
                      style: const TextStyle(fontSize: 16)),
                  Expanded(
                    child: Text("${offer.price}\$",
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.deepPurple),
                        textAlign: TextAlign.end),
                  ),
                ])),
            const Divider(
              thickness: 2.0,
              color: Colors.transparent,
            )
          ],
        );
  }
}
