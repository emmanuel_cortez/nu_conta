import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:nu_conta_task/screens/data/graphQl/data_base_client.dart';
import 'package:nu_conta_task/screens/views/screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
        client: getDBClient(),
        child: MaterialApp(
      title: 'NuConta Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        secondaryHeaderColor: Colors.deepPurpleAccent
      ),
      home: const HomePage(title: 'NuConta Demo Home Page'),
    ));
  }
}


